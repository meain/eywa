'''
Abstract class Machine
f(x) = x
Simply copies inputs to outputs
'''
class Machine:

	def __init__(self, frequency=1, run_on_input=True):

		self.frequency = frequency
		self.run_on_input = run_on_input
		self.outputs = []
		self.inputs = []

	def _run(self):

		outputs += inputs

	def run(self):

		i = 0
		while(i < frequency):
			self._run()
			i+=1

	def input(self, x):

		self.inputs.append(x)
		if self.run_on_input:
			self.run()

	def multi_input(self, x):

		self.inputs += x
		if self.run_on_input:
			self.run()

	def get_outputs(self):
		outputs = self.outputs[:]
		self.outputs =  []
		return outputs


from collections import OrderedDict
import json
import gensim

class W2V:

	def __init__(self, file = None):

		self.embeddings = OrderedDict()
		self.file = file
		if file:
			self.load(file)
		

	def load(self, file):

		self.file = file
		pass

	def save(self, file = None):

		if not file:
			if self.file:
				file = self.file
			else:
				raise Exception('File not specified')

	def get(self, word):
		return None

	def set(self, word, e):
		pass
